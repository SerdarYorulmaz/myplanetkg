package com.example.myplanetkg

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.CheckBox
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

interface ConvertFunction {
    fun convertToPound(numA: Double): Double
    fun convertToKg(numA: Double): Double
}

class MainActivity : AppCompatActivity(), ConvertFunction, View.OnClickListener {


    val POUND_CONVERT = 2.2045 //sabit
    val KG_CONVERT = 0.45359237
    val MARS = 0.38
    val VENUS = 0.91
    val JUPITER = 2.34

    override fun onSaveInstanceState(outState: Bundle) { //yatay  cevrildiginde verinin kaybolmasin diye
        super.onSaveInstanceState(outState)
        outState.putString("sonuc",tvResult.text.toString())
    }
    //deneme
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //activite kulanacagimiz icin this dedik load -> nerden alacam ->nereye verecem ->into
        Glide.with(this).load(R.drawable.rosee).into(imageView)

        tvResult.text=savedInstanceState?.getString("sonuc")
        //nelerin tıklanmasin onemsedigimiz daha belirtmedik asagida belirtecegiz
        checkBoxMars.setOnClickListener(this)
        checkBoxJupiter.setOnClickListener(this)
        checkboxVenus.setOnClickListener(this)


        // var myKg = etKg.text

        //uzun yol
        /*btnCal.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                Log.e("Hesapla","Butona basildi")
            }
        })*/
        //asaida zaten yaptik bunu icin iptal ediyoruz
        /* btnCal.setOnClickListener {
             tvResult.setText("")

             var convertPound = convertToPound((myKg.toString()).toDouble())
             var marsPound = convertPound * MARS
             var poundConvertKg = convertToKg(marsPound)

             tvResult.text = (poundConvertKg.format(2))

         }*/
    }

    override fun convertToPound(numA: Double): Double {

        return (numA * POUND_CONVERT)

    }

    override fun convertToKg(numA: Double): Double {

        return (numA * KG_CONVERT)

    }

    override fun onClick(v: View?) {
        //yukarda button icin tanimlamista burda dinleyecegimiz checkbox


        var isChecked: Boolean = (v as CheckBox).isChecked





        if (!TextUtils.isEmpty(etKg.text.toString())) {
            var userKg = (etKg.text.toString()).toDouble()
            var kgToPound = convertToPound(userKg)
            //TextUtils.isEmpty(etKg.text.toString()) icindeki ifadenin bos sa true donduruyor
            //tek tiklama icin radyo buton kullanmak daha mantikli
            when (v.id) {
                R.id.checkBoxMars -> if (isChecked) {
                    checkBoxJupiter.isChecked = false
                    checkboxVenus.isChecked = false
                    calculatePound(kgToPound, v)
                }
                R.id.checkBoxJupiter -> if (isChecked) {
                    checkBoxMars.isChecked = false
                    checkboxVenus.isChecked = false
                    calculatePound(kgToPound, v)
                }
                R.id.checkboxVenus -> if (isChecked) {
                    checkBoxJupiter.isChecked = false
                    checkBoxMars.isChecked = false
                    calculatePound(kgToPound, v)
                }
            }
        }


    }

    fun calculatePound(pound: Double, checkBox: CheckBox) {

        var resultKg: Double = 0.0

        when (checkBox.id) {

            R.id.checkBoxMars -> resultKg = pound * MARS

            R.id.checkboxVenus -> resultKg = pound * VENUS

            R.id.checkBoxJupiter -> resultKg = pound * JUPITER

            else -> resultKg = 0.0
        }
        var result = convertToKg(resultKg)
        tvResult.text = result.format(2)
    }

    fun Double.format(escapeNumber: Int) = java.lang.String.format("%.${escapeNumber}f", this)


}
